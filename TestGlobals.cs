﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Models;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;

namespace OntologyWebAppTests
{
    public class TestGlobals
    {
        private clsTypes types = new clsTypes();
        private WebsocketServiceAgent websocketServiceAgent;
        public WebsocketServiceAgent WebsocketServiceAgent
        {
            get { return websocketServiceAgent; }
        }

        public void SetWebApp()
        {
            OntoMsg_Module.ModuleDataExchanger.CurrentWebApp = GetWebAppItem();
            OntoMsg_Module.ModuleDataExchanger.UserGroupPath = GetUserPath();
        }


        public clsOntologyItem GetUser()
        {
            return new clsOntologyItem
            {
                GUID = "74f38566eda64c76b862e839b6a8d07d",
                Name = "TassiloK.Prj",
                GUID_Parent = "c441518dbfe04d55b538df5c5555dd83",
                Type = types.ObjectType
            };
        }

        public clsOntologyItem GetGroup()
        {
            return new clsOntologyItem
            {
                GUID = "ac020d4ec1574a22a1c79e874475a4ab",
                Name = "Home-Work",
                GUID_Parent = "1b1f843c19b74ae1a73f6191585ec5c6",
                Type = types.ObjectType
            };
        }

        public void OpenChannel()
        {
            CreateWebSocketServiceAgent();
            

            websocketServiceAgent.DataText_SessionId = Guid.NewGuid().ToString();
           
        }

        public WebSocket GetWebSocket()
        {
            return new WebSocket("wss://localhost:6402", "http");
        }

        public string GetUserPath()
        {
            return @"C:\UserGroupRessources";
        }

        public void CreateWebSocketServiceAgent()
        {
            websocketServiceAgent = new OntoMsg_Module.WebSocketServices.WebsocketServiceAgent();
            websocketServiceAgent.oItemUser = GetUser();
            websocketServiceAgent.oItemGroup = GetGroup();

            var propWebSocket = websocketServiceAgent.GetType().GetField("webSocketComServerClient", BindingFlags.NonPublic | BindingFlags.Instance);
            var websocket = GetWebSocket();
            propWebSocket.SetValue(websocketServiceAgent, websocket);
        }

        public WebAppItem GetWebAppItem()
        {
            return new OntoMsg_Module.Models.WebAppItem
            {
                IdEntryView = "9f966e30c4734b9d82a23ddf98bbeae0",
                EntryUrl = "https://www.omodules.de/OntologyModules",
                NameEntryView = "AppointmentsCalendar",
                EntrySessionPath = @"C:\inetpub\vhosts\omodules.de\httpdocs\Resources"
            };
        }
    }
}
