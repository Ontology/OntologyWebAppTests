﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using OntoMsg_Module.WebSocketServices;
using AppointmentModule.Controllers;

namespace OntologyWebAppTests
{
    [TestClass]
    public class AppointmentTests
    {
        private TestGlobals testGlobals = new TestGlobals();
        
        private AppointmentController appointmentController;

        [TestMethod]
        public void Appointment_CTOR_Normal()
        {
            var appointmentController = new AppointmentModule.Controllers.AppointmentController();

        }

        

        private void CreateAppointmentController()
        {
            appointmentController = new AppointmentModule.Controllers.AppointmentController();
            appointmentController.InitializeViewController(testGlobals.WebsocketServiceAgent);

            var method = appointmentController.GetType().GetMethod("StateMachine_openedSocket", BindingFlags.NonPublic | BindingFlags.Instance);
            method.Invoke(appointmentController, null);
        }

        

        private void SetLoginSuccess(bool success)
        {
            var method = appointmentController.GetType().GetMethod("StateMachine_loginSucceded", BindingFlags.NonPublic | BindingFlags.Instance);
            method.Invoke(appointmentController, null);
        }

      
        [TestMethod]
        public void Appointment_ChannelOpened()
        {
            testGlobals.OpenChannel();
            CreateAppointmentController();

            if (string.IsNullOrEmpty(appointmentController.Text_View))
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Appointment_GetAppointmentsOfUser()
        {
            testGlobals.OpenChannel();
            testGlobals.SetWebApp();
            var propWebSocket = testGlobals.WebsocketServiceAgent.GetType().GetProperty("IdEntryView", BindingFlags.Public | BindingFlags.Instance);

            // Set Appointment-Calender-View
            propWebSocket.SetValue(testGlobals.WebsocketServiceAgent, testGlobals.GetWebAppItem().IdEntryView);

            var serviceAgentElasticField = appointmentController.GetType().GetField("serviceAgentElastic", BindingFlags.NonPublic | BindingFlags.Instance);
            AppointmentModule.Services.ServiceAgentElastic serviceAgent = (AppointmentModule.Services.ServiceAgentElastic) serviceAgentElasticField.GetValue(appointmentController);

            if (serviceAgent == null)
            {
                Assert.Fail();
            }
            
            SetLoginSuccess(true);
            

        }

    }
}
