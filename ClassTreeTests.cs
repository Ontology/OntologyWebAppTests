﻿using ClassTreeController.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OntologyWebAppTests
{
    [TestClass]
    public class ClassTreeTests
    {
        private TestGlobals testGlobals = new TestGlobals();
        private WebsocketServiceAgent websocketServiceAgent;
        private ClassTreeViewController classTreeViewController;
        

        [TestMethod]
        public void ClassTree_CTOR_Normal()
        {
            var classTreeController = new ClassTreeViewController();


        }

        [TestMethod]
        public void ClassTree_ChannelOpened()
        {
            testGlobals.OpenChannel();
            CreateClassTreeController();

            if (string.IsNullOrEmpty(classTreeViewController.Text_View))
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClassTree_GetClassTree()
        {
            
            
            testGlobals.OpenChannel();
            CreateClassTreeController();
            testGlobals.SetWebApp();

            var method = classTreeViewController.GetType().GetMethod("StateMachine_loginSucceded", BindingFlags.NonPublic | BindingFlags.Instance);
            method.Invoke(classTreeViewController, null);

            if (classTreeViewController.PathNodes == null || !classTreeViewController.PathNodes.Any())
            {
                Assert.Fail();
            }

            Thread.Sleep(1000);

            if (string.IsNullOrEmpty(classTreeViewController.Url_TreeData))
            {
                Assert.Fail();
            }
        }

        private void CreateClassTreeController()
        {
            classTreeViewController = new ClassTreeViewController();
            classTreeViewController.InitializeViewController(testGlobals.WebsocketServiceAgent);

            var method = classTreeViewController.GetType().GetMethod("StateMachine_openedSocket", BindingFlags.NonPublic | BindingFlags.Instance);
            method.Invoke(classTreeViewController, null);
        }


    }
}
